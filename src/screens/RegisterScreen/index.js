import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import React, { useEffect } from 'react';
import {
  ImageBackground, ScrollView, StyleSheet, Text, View,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { BcLogin } from '../../assets';
import {
  ButtonComponent, Input, LinkComponent,
} from '../../components';
import { setLoading } from '../../redux';
import {
  colors, fonts, onLogScreenView, showError, showSuccess, storeData, useForm,
} from '../../utils';
import { windowHeight, windowWidth } from '../../utils/Dimensions';

export default function RegisterScreen({ navigation }) {
  const dispatch = useDispatch();
  const [form, setForm] = useForm({
    fullname: '',
    email: '',
    password: '',
  });

  useEffect(() => {
    onLogScreenView('RegisterScreen');
  }, []);

  const validateEmail = (text) => {
    setForm('email', text);
  };

  const validatePassword = (text) => {
    setForm('password', text);
  };

  const validateFullname = (text) => {
    setForm('fullname', text);
  };

  const registerUser = () => {
    dispatch(setLoading(true));
    auth()
      .createUserWithEmailAndPassword(form.email, form.password)
      .then((res) => {
        const data = {
          fullname: form.fullname,
          email: form.email,
          uid: res.user.uid,
        };

        const dataLocal = {
          fullname: form.fullname,
          email: form.email,
          uid: res.user.uid,
          password: form.password,

        };
        dispatch(setLoading(false));
        setForm('reset');
        database()
          .ref(`users/${res.user.uid}/`)
          .set(data);

        storeData('user', dataLocal);
        showSuccess('Register Success');
        navigation.navigate('LoginScreen');
      })
      .catch((err) => {
        dispatch(setLoading(false));
        showError(err.message);
      });
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
      <View style={styles.pages}>
        <ImageBackground source={BcLogin} style={styles.image}>
          <View style={styles.registerWrapper}>
            <Text style={styles.title}>
              Register
            </Text>
            <Input label="Full Name" onChangeText={(text) => validateFullname(text)} value={form.fullname} visible={form.fullname.length <= 0} />
            <Input label="Email" onChangeText={(text) => validateEmail(text)} value={form.email} visible={form.email.length <= 0} />
            <Input
              label="Password"
              onChangeText={(text) => validatePassword(text)}
              value={form.password}
              visible={form.password.length <= 0}
            />

          </View>
          <View style={styles.goRegisterWrapper}>
            <Text style={styles.registerTitle}>
              Already Member?
              {' '}
            </Text>
            <LinkComponent title="Login" color={colors.text.primary} size={16} onPress={() => navigation.navigate('LoginScreen')} />
          </View>
          <View style={styles.btnWrapper}>
            <ButtonComponent label="Register" onPress={() => registerUser()} disable={!(form.password && form.email && form.fullname)} />
          </View>
        </ImageBackground>
      </View>
    </ScrollView>

  );
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
  },

  image: {
    width: windowWidth,
    height: windowHeight,
  },

  registerWrapper: {
    paddingLeft: 21,
    position: 'absolute',
    top: windowHeight / 3 - 40,
    width: windowWidth - 21,
  },
  title: {
    fontSize: 36,
    fontFamily: fonts.primary[800],
    color: colors.text.secondary,
    marginBottom: 10,
  },

  btnWrapper: {
    position: 'absolute',
    bottom: 30,
    width: windowWidth / 3,
    right: windowWidth / 10,

  },

  iconWrapper: {
    marginTop: 16,
    flexDirection: 'row',
    width: windowWidth / 3,
    justifyContent: 'space-between',
  },
  linkWrapper: {
    marginTop: -30,
    alignItems: 'flex-end',
    marginLeft: windowWidth / 2,
  },
  goRegisterWrapper: {
    position: 'absolute',
    bottom: 10,
    left: 21,
    flexDirection: 'row',
  },
  registerTitle: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.text.primary,
  },
});
