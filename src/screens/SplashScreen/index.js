/* eslint-disable global-require */
import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import LottieView from 'lottie-react-native';
import { COLORS, FONTS } from '../../themes';

export default function Splash({ navigation }) {
  return (
    <View style={styles.container}>
      <LottieView
        source={require('../../assets/json/SplashAnim.json')}
        autoPlay
        loop={false}
        onAnimationFinish={() => {
          navigation.replace('LoginScreen');
        }}
      />
      <Text style={styles.copyright}>Dzun Nurroin</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  copyright: {
    ...FONTS.body4,
    color: COLORS.gray,
    position: 'absolute',
    bottom: 19,
    letterSpacing: 7,
  },
});
