/* eslint-disable global-require */
import analytics from '@react-native-firebase/analytics';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import LottieView from 'lottie-react-native';
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import {
  Alert,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import { useDispatch, useSelector } from 'react-redux';
import TouchID from 'react-native-touch-id';
import GIcon from '../../assets/icons/ic_google.svg';
import { CustomButton, CustomInput } from '../../components';
import { login, signInSocialMedia } from '../../config';
import { setLoading } from '../../redux';
import { COLORS, FONTS, SIZES } from '../../themes';
import {
  EMAIL_REGEX, getData, onLogScreenView, PASSWORD_REGEX, showError, showSuccess, storeData,
} from '../../utils';

export default function Login({ navigation }) {
  const [isSecureEntry, setIsSecureEntry] = useState(true);
  const stateGlobal = useSelector((state) => state);
  const dispatch = useDispatch();
  const { control, handleSubmit } = useForm();

  const [isLogin, setIsLogin] = useState(false);

  useEffect(() => {
    onLogScreenView('LoginScreen');
    GoogleSignin.configure({
      webClientId: '852627795578-vlhu1s75so611fd0hcdi5aok8u20atq8.apps.googleusercontent.com',
    });

    getData('user').then((user) => {
      if (user !== null) {
        setIsLogin(true);
      } else {
        setIsLogin(false);
      }
    });
  }, []);

  const loginUser = (data) => {
    dispatch(setLoading(true));
    login(data.email, data.password)
      .then(() => {
        analytics().logEvent('Login', {
          method: 'email_password',
        });
        analytics().setUserProperty('Login_with', 'email_password');
        dispatch(setLoading(false));
        storeData('user', data);
        navigation.replace('MainApp');
        showSuccess('Login Success');
      })
      .catch((err) => {
        dispatch(setLoading(false));
        showError(err.message);
      });
  };

  const onFingerPrintPress = () => {
    const optionalConfigObject = {
      title: 'Authentication Required',
      imageColor: '#e00606',
      imageErrorColor: '#ff0000',
      sensorDescription: 'Touch sensor',
      sensorErrorDescription: 'Failed',
      cancelText: 'Cancel',
      fallbackLabel: 'Show Passcode',
      unifiedErrors: false,
      passcodeFallback: false,
    };

    TouchID.isSupported(optionalConfigObject).then((biometryType) => {
      if (biometryType === 'FaceID') {
        Alert.alert('This device supports FaceID');
      } else {
        TouchID.authenticate('To access your account', optionalConfigObject)
          .then(() => {
            dispatch(setLoading(true));
            getData('user').then((user) => {
              if (user) {
                const users = {
                  email: user.email,
                  password: user.password,
                };
                login(users.email, users.password)
                  .then(() => {
                    analytics().logEvent('Login', {
                      method: 'Biometric',
                    });
                    analytics().setUserProperty('Login_with', 'Biometric');
                    dispatch(setLoading(false));
                    showSuccess('Login Success');
                    navigation.replace('MainApp');
                  }).catch((err) => {
                    dispatch(setLoading(false));
                    showError(err.message);
                  });
              } else {
                dispatch(setLoading(false));
                showError('Please Login first');
              }
            });
          })
          .catch((error) => {
            showError(error.message);
          });
      }
    });
  };

  async function onGoogleButtonPress() {
    dispatch(setLoading(true));
    // Get the users ID token
    const { idToken } = await GoogleSignin.signIn();

    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    return signInSocialMedia(googleCredential);
  }

  return (
    <ScrollView contentContainerStyle={styles.scroll} testID="LoginScreen">
      <View style={styles.container}>
        <LottieView
          style={styles.logoImage}
          source={require('../../assets/json/Logo.json')}
          autoPlay
          loop={false}
        />
        <View>
          {/* <Text style={styles.title}>Welcome to Book Store</Text> */}
          {/* <Text style={styles.subTitle}>Please login here</Text> */}
          <View style={styles.form}>
            <CustomInput
              testID="input-email"
              label="Email"
              name="email"
              iconPosition="right"
              placeholder="Enter Email"
              control={control}
              rules={{
                required: 'Email is required',
                pattern: { value: EMAIL_REGEX, message: 'Email is invalid' },
              }}
            />
            <CustomInput
              testID="input-password"
              label="Password"
              name="password"
              iconPosition="right"
              secureTextEntry={isSecureEntry}
              control={control}
              rules={{
                required: 'Password is required',
                minLength: {
                  value: 8,
                  message: 'Password must be at least 8 characters',
                },
                pattern: {
                  value: PASSWORD_REGEX,
                  message:
                    'Password must be contain at least 1 letter and 1 number',
                },
              }}
              placeholder="Enter Password"
              icon={(
                <TouchableOpacity
                  onPress={() => {
                    setIsSecureEntry((prev) => !prev);
                  }}
                >
                  <Icon
                    name={isSecureEntry ? 'eye-off' : 'eye'}
                    size={24}
                    color={COLORS.gray}
                  />
                </TouchableOpacity>
              )}
            />
            <CustomButton testID="btn-login" loading={stateGlobal.isLoading} primary title="Login" onPress={handleSubmit(loginUser)} />
            {
              isLogin && (
                <CustomButton testID="btn-login" loading={stateGlobal.isLoading} danger title="Login by Finger Print" onPress={() => onFingerPrintPress()} />
              )
            }

            <View style={styles.createSection}>
              <Text style={styles.infoText}>- or -</Text>
            </View>

            <CustomButton
              testID="btn-login"
              secondary
              icon={<GIcon />}
              title="Login with Google"
              onPress={() => onGoogleButtonPress()
                .then((res) => {
                  analytics().logEvent('Login', {
                    method: 'Google',
                  });
                  analytics().setUserId(res.user.uid);
                  analytics().setUserProperty('Login_with', 'Google');
                  const data = {
                    fullname: res.user.displayName,
                    email: res.user.email,
                    uid: res.user.uid,
                  };
                  dispatch(setLoading(false));
                  showSuccess('Login Sukses');
                  navigation.replace('MainApp');
                  database()
                    .ref(`users/${res.user.uid}/`)
                    .set(data);
                })
                .catch((err) => {
                  dispatch(setLoading(false));
                  showError(err.message);
                })}
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  backgroundVideo: {
    width: '100%',
    height: 190,
    borderRadius: 10,
  },
  scroll: { flexGrow: 1, backgroundColor: COLORS.white },
  container: {
    padding: 24,
    flex: 1,
    backgroundColor: COLORS.white,
  },
  logoImage: {
    height: SIZES.width * 0.7,
    width: SIZES.width * 0.7,
    alignSelf: 'center',
    // marginTop: 50,
  },
  title: {
    ...FONTS.h2,
    textAlign: 'center',
    paddingTop: 20,
    color: COLORS.black,
  },

  subTitle: {
    fontSize: 17,
    textAlign: 'center',
    paddingVertical: 20,
    fontWeight: '500',
  },

  form: {
    paddingTop: 15,
  },
  createSection: {
    marginVertical: 2,
    justifyContent: 'center',
    alignItems: 'center',
    // flexDirection: 'row',
  },
  linkBtn: {
    marginTop: 7,
    color: COLORS.primary,
    ...FONTS.h3,
  },

  infoText: {
    ...FONTS.body4,
  },
});
