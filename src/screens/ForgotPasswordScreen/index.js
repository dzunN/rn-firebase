import React, { useEffect } from 'react';
import {
  ImageBackground, ScrollView, StyleSheet, Text, View,
} from 'react-native';
import { useDispatch } from 'react-redux';
import { BcLogin } from '../../assets';
import {
  ButtonComponent, Input, LinkComponent,
} from '../../components';
import { forgetPassword } from '../../config';
import { setLoading } from '../../redux';
import {
  colors, fonts, onLogScreenView, showError, showSuccess, useForm,
} from '../../utils';
import { windowHeight, windowWidth } from '../../utils/Dimensions';

export default function ForgotPasswordScreen({ navigation }) {
  const dispatch = useDispatch();
  const [form, setForm] = useForm({
    email: '',
  });

  useEffect(() => {
    onLogScreenView('ForgotPasswordScreen');
  }, []);

  const validateEmail = (text) => {
    setForm('email', text);
  };

  const handlePasswordReset = () => {
    dispatch(setLoading(true));
    forgetPassword(form.email).then(() => {
      dispatch(setLoading(false));
      showSuccess('Please check your email');
      navigation.navigate('LoginScreen');
    }).catch((err) => {
      dispatch(setLoading(false));
      showError(err.message);
    });
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false} keyboardShouldPersistTaps="handled">
      <View style={styles.pages}>
        <ImageBackground source={BcLogin} style={styles.image}>
          <View style={styles.loginWrapper}>
            <Text style={styles.title}>
              Forgot Password?
            </Text>
            <Input label="Email" onChangeText={(text) => validateEmail(text)} value={form.email} visible={form.email.length <= 0} />
          </View>
          <View style={styles.goRegisterWrapper}>
            <Text style={styles.registerTitle}>
              Back to Login?
              {' '}
            </Text>
            <LinkComponent title="Login" color={colors.text.primary} size={16} onPress={() => navigation.navigate('LoginScreen')} />
          </View>
          <View style={styles.btnWrapper}>
            <ButtonComponent label="Send" onPress={() => handlePasswordReset()} disable={!(form.email)} />
          </View>
        </ImageBackground>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  pages: {
    flex: 1,
  },

  image: {
    width: windowWidth,
    height: windowHeight,
  },

  loginWrapper: {
    paddingLeft: 21,
    position: 'absolute',
    top: windowHeight / 3,
    width: windowWidth - 21,
  },
  title: {
    fontSize: 36,
    fontFamily: fonts.primary[800],
    color: colors.text.secondary,
    marginBottom: 10,
  },

  btnWrapper: {
    position: 'absolute',
    bottom: 30,
    width: windowWidth / 3,
    right: windowWidth / 10,

  },

  iconWrapper: {
    marginTop: 16,
    flexDirection: 'row',
    width: windowWidth / 3,
    justifyContent: 'space-between',
  },
  linkWrapper: {
    marginTop: -30,
    alignItems: 'flex-end',
    marginLeft: windowWidth / 2,
  },
  goRegisterWrapper: {
    position: 'absolute',
    bottom: 10,
    left: 21,
    flexDirection: 'row',
  },
  registerTitle: {
    fontFamily: fonts.primary[600],
    fontSize: 16,
    color: colors.text.primary,
  },
});
