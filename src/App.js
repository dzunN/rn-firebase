import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { Provider, useSelector } from 'react-redux';
import FlashMessage from 'react-native-flash-message';
import { StatusBar } from 'react-native';
import Router from './router';
import { Loading } from './components';
import { Store } from './redux';
import { colors } from './utils';

function MainApp() {
  const stateGlobal = useSelector((state) => state);
  return (
    <>
      <StatusBar barStyle="light-content" backgroundColor={colors.background.secondary} />
      <NavigationContainer>
        <Router />
      </NavigationContainer>
      <FlashMessage position="top" />
      {stateGlobal.isLoading && <Loading />}
    </>
  );
}
function App() {
  return (
    <Provider store={Store}>
      <MainApp />
    </Provider>
  );
}

export default App;
